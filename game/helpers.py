import math

def magSq(vector):
    x, y = vector[0], vector[1]
    return (x ** 2) + (y ** 2)      

def limit(vector, m):
    mSq = magSq(vector)
    if mSq > m * m:
        vector = vector / math.sqrt(mSq)
        vector *= m
    return vector