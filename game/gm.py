import pygame as pg
import random
from game.config import Screen, Colors
from game.entities import Host, Food
from game.gui import GUI, Label

class GameManager:
    def __init__(self):
        pg.init()
        pg.font.init()
        pg.mixer.init()

        self.screen = pg.display.set_mode(
            size=Screen.RESOLUTION)
        self.clock = pg.time.Clock()
        self.gui = GUI()
        self.running = True
        self.pausing = False

    def init(self):
        self.entities = pg.sprite.LayeredUpdates()
        self.food = pg.sprite.Group()
        
        for _ in range(3000):
            self.food.add(Food(
                x=random.uniform(
                    Screen.BOUNDARY_SIZE, 
                    Screen.WIDTH - Screen.BOUNDARY_SIZE),
                y=random.uniform(
                    Screen.BOUNDARY_SIZE, 
                    Screen.HEIGHT - Screen.BOUNDARY_SIZE),
            ))

        for _ in range(20):
            self.entities.add(Host(
                x=random.uniform(0, Screen.WIDTH),
                y=random.uniform(0, Screen.HEIGHT),
                food=self.food
            ))

    def update(self):
        self.entities.update()
        self.food.update()
        self.gui.fps_label.set_text(f"FPS: {self.clock.get_fps():.0f}")

    def draw(self):
        self.screen.fill(Colors.BGCOLOR)

        self.entities.draw(self.screen)
        self.food.draw(self.screen)

        self.gui.draw(self.screen)

        pg.display.flip()

    def events(self):
        for event in pg.event.get():
            if event.type == pg.QUIT:
                if self.running:
                    self.running = False
            elif event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                        self.running = False
    def run(self):
        while self.running:
            self.clock.tick(Screen.FPS)
            self.events()
            self.draw()
            self.update()